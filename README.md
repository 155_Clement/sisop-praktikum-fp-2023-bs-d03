### Folder screenshot : https://drive.google.com/drive/folders/1gbmpwXqovwouCamK-QcisaxYb7wfjbhH?usp=sharing

### Masalah untuk containerization : Belum menemukan cara agar client di container dan database di host bisa berkomunikasi dengan socket.

# client.c

- Periksa sudo menggunakan `getuid`.
- Cek apakah argumen menandakan program dijalankan menggunakan redirection atau tidak menggunakan jumlah argumen.
- Buat socket lalu dihubungkan dengan database.
- Jika merupakan redirection, kirim perintah `USE` untuk mengubah working database sesuai argumen.
- LOOPING membaca dan mengirim instruksi ke database, dan menerima hasil dari database.


# database.c (Bagian database selain fungsi seputar akun user)

## 1. DEFINE dan STRUCT
```
#define PORT 8080

#define MAXDATALEN 200
#define MAXCOLUMN 100

#define DBROOT "databases"
#define NO_DB "NULL"
```
- PORT : Port untuk socket.
- MAXDATALEN : Panjang maksimal sebuah data dalam kolom di dalam database.
- MAXCOLUMN : Jumlah maksimal kolom setiap tabel.
- DBROOT : Nama folder yang berisi setiap database (sesuai soal).
- NO_DB : Nama database default ketika user belum memilih/membuat database apapun.
```
typedef struct tableStruct
{
    int columnAmount;
    int rowAmount;
    char data[MAXCOLUMN][MAXDATALEN]; // Max 100 column, data max 200
    char type[MAXCOLUMN][10];         // String or int
} tabledata;

struct userStruct
{
    char name[MAXDATALEN];
    char password[MAXDATALEN];
};

struct permissionStruct
{
    char database[MAXDATALEN];
    char name[MAXDATALEN];
};
```
- tabledata : Struct yang digunakan untuk menyimpan data sebuah tabel.
    - columnAmount : Jumlah kolom tabel tersebut.
    - rowAmount : Jumlah baris data tabel tersebut.
    - data : Array 2D yang menyimpan data sebagai string seperti format csv :
        - index 0 : "`namakolom1;namakolom2;...;namakolomN;`".
        - index 1-M : "`data1;data2;...;dataN;`".
    - type : Array 2D yang menyimpan tipe data setiap kolom :
        - index 1 : Tipe data kolom 1
        - index 2 : Tipe data kolom 2
        - dst
- userStruct : Struct yang digunakan untuk menyimpan sementara informasi username dan password. Isi dibaca dari file `./databases/user.csv`.
- permissionStruct : Struct yang digunakan untuk menyimpan sementara informasi username dan database yang boleh diakses user tersebut. Isi dibaca dari file `./databases/permission.csv`.

## 2. CREATE
### A - DATABASE

- Fungsi : Membuat folder baru untuk database dengan permission umum.
- Argumen : 
    - database : nama database baru.
- Cara kerja :
    - Membangun path folder baru menggunakan `sprintf`.
    - Memanggil `mkdir`, mengembalikan return value untuk error checking di `main`.

### B - TABLE

- Fungsi : Membuat file yang berisi data database kosong sesuai argumen yang diberikan.
- Argumen :
    - database : Database lokasi dibuatnya tabel baru.
    - table : Nama tabel baru.
    - columns : String berisi nama dan tipe kolom. Format : `([nama_kolom1] [tipe_data1], [nama_kolom2] [tipe_data2], ...);`
- Cara kerja :
    - Buat path database, lalu periksa apakah folder tersebut bisa diakses/ada atau tidak menggunakan `opendir`. Keluar jika tidak bisa.
    - Buat path untuk file, lalu periksa apakah file yang sama sudah ada atau tidak menggunakan `access`. Keluar jika sudah ada.
    - Siapkan struct `tabledata` lalu olah data **columns**. Gunakan `strtok` untuk memotong **columns** menjadi substring dengan `',' dan ' '` sebagai delimiter. Simpan nama dan tipe kolom dalam struct.
    - Buat file **binary** (Karena menyimpan struct) dengan `fopen`. Cek apakah berhasil, jika tidak keluar.
    - Tulis data struct ke dalam file menggunakan `fwrite`, lalu tutup.
    - Cek apakah `fwrite` berhasil, return nilai sesuai untuk error checking.

## 3. DROP
### A - DATABASE
- Fungsi : Menghapus folder database beserta isinya.
- Argumen :
    - database : nama database yang dihapus.
- Cara kerja :
    - Buat path database, lalu periksa apakah folder tersebut bisa diakses/ada atau tidak menggunakan `opendir`. Keluar jika tidak bisa.
    - Gunakan `nftw` untuk menelusuri directory. Gunakan fungsi `rmFiles` untuk menghapus isi directory sebelum menghapus folder itu sendiri.
    - Cek untuk error ntfw.
### B - TABLE
- Fungsi : Menghapus file tabel.
- Argumen :
    - database : Database lokasi dihapusnya tabel.
    - table : Nama tabel yang didrop.
- Cara kerja :
    - Buat path database, lalu periksa apakah folder tersebut bisa diakses/ada atau tidak menggunakan `opendir`. Keluar jika tidak bisa.
    - Buat path untuk file, lalu periksa apakah file ada atau tidak menggunakan `access`. Keluar jika tidak ditemukan.
    - Hapus file menggunakan `remove`, cek untuk error.
### C - COLUMN
- Fungsi : Menghapus kolom suatu tabel dengan menghapus nama dan data kolom tersebut dari tabel.
- Argumen :
    - database : Database lokasi tabel.
    - table : Nama tabel yang dihapus kolomnya.
    - columns : String berisi nama kolom yang dihapus.
- Cara kerja :
    - Buat path database, lalu periksa apakah folder tersebut bisa diakses/ada atau tidak menggunakan `opendir`. Keluar jika tidak bisa.
    - Buat path untuk file, lalu periksa apakah file ada atau tidak menggunakan `access`. Keluar jika tidak ditemukan.
    - Baca dan simpan data dalam file menggunakan `fread`.
    - Menggunakan `strtok`, copy lalu cek daftar kolom tabel. Cari index kolom yang ingin dihapus (menggunakan index 1,2,3,...). Simpan di `columnNum`.
    - Looping untuk setiap baris `data` dalam struct, termasuk nama kolom :
        - Copy baris, siapkan row baru.
        - Gunakan `strtok` dengan delimiter `';'` untuk mencari nilai yang dihapus (hitung index substring, dibandingkan dengan `columnNum`).
        - Tuliskan data kolom SELAIN kolom yang dihapus ke row `tempRow`.
        - Overwrite data row lama dengan `tempRow`.
    - Buka file tabel dengan `fopen` dengan mode `wb` agar isi file terhapus. Simpan struct yang diubah ke dalam file tersebut menggunakan `fwrite`.
    - Cek return value `fwrite`.
## 4. INSERT
- Fungsi : Menambahkan baris data ke dalam tabel.
- Argumen :
    - database : Database lokasi tabel.
    - table : Nama tabel.
    - columns : String berisi data per kolom. Format : `([value1], [value2], ...);`
- Cara kerja :
    - Buat path database, lalu periksa apakah folder tersebut bisa diakses/ada atau tidak menggunakan `opendir`. Keluar jika tidak bisa.
    - Buat path untuk file, lalu periksa apakah file ada atau tidak menggunakan `access`. Keluar jika tidak ditemukan.
    - Baca dan simpan data dalam file menggunakan `fread`. 
    - Menggunakan `strtok`, cek setiap data yang ingin dimasukkan dan simpan baris hasil di `data` struct untuk index baris baru menggunakan `strcat`. Untuk data jenis `string`, simpan dalam `temp` lalu hapus karakter `'` sebelum digabungkan ke dalam baris menggunakan `memmove` untuk menggeser data ke depan (`'data'` -> `data''`) lalu tandai end of line (`data''` -> `data\0''`).
    - Buka file tabel dengan `fopen` dengan mode `wb` agar isi file terhapus. Simpan struct yang diubah ke dalam file tersebut menggunakan `fwrite`.
    - Cek return value `fwrite`.
## 5. SELECT
- Fungsi : Membaca data tabel.
- Argumen :
    - database : Database lokasi tabel.
    - table : Nama tabel.
    - all : Boolean, apakah diminta semua kolom atau tidak.
    - columns : Array 2D berisi nama kolom yang diminta datanya.
    - isWhere : Boolean, apakah ada syarat WHERE atau tidak.
    - whereColumn : Nama kolom yang dijadikan syarat WHERE.
    - whereValue : Nilai yang dijadikan syarat WHERE.
    - sendS : Pointer untuk string yang akan dikirim ke client.
- Cara kerja :
    - Buat path database, lalu periksa apakah folder tersebut bisa diakses/ada atau tidak menggunakan `opendir`. Keluar jika tidak bisa.
    - Buat path untuk file, lalu periksa apakah file ada atau tidak menggunakan `access`. Keluar jika tidak ditemukan.
    - Baca dan simpan data dalam file menggunakan `fread`. 
    - JIKA `isWhere == 1` :
        - Copy data kolom di tabel, lalu pakai `strtok` dan `strcmp` untuk mencari index `whereColumn`.
        - Jika `whereColumn` memiliki tipe data `string`, cek formatting `whereValue`. Jika benar, hapus karakter `'` menggunakan `memmove` untuk menggeser data ke depan (`'data'` -> `data''`) lalu tandai end of line (`data''` -> `data\0''`).
    - Siapkan array untuk hasil PER BARIS dari operasi SELECT yaitu `temp`.
    - JIKA `all == 1` :
        UNTUK SETIAP BARIS DATA

        - JIKA `isWhere == 1` :
            - Copy baris, gunakan `strtok` dan `strcmp` untuk membandingkan nilai di kolom yang sama dengan `whereColumn` dengan `whereValue`.
            - Untuk baris yang nilainya sama, cetak ke `temp` dengan diberi nomor baris lalu tambahkan ke `sendS` menggunakan `strcat`.
        - ELSE :
            - Tambahkan semua baris ke `sendS` menggunakan `strcat`.
    - ELSE, jika ada kolom tertentu yang diminta :
        - Copy data kolom di tabel, lalu pakai `strtok` dan `strcmp` untuk mencari index kolom-kolom di `columns`. Index akan disimpan kembali di dalam `columns`.
        - Jika ada kolom di `columns` memiliki tipe data `string`, cek formatting `whereValue`. Jika benar, hapus karakter `'` menggunakan `memmove` untuk menggeser data ke depan (`'data'` -> `data''`) lalu tandai end of line (`data''` -> `data\0''`).

        LOOP UNTUK SETIAP BARIS DATA


        - JIKA `isWhere == 1` :
            - Copy baris, gunakan `strtok` dan `strcmp` untuk membandingkan nilai di kolom yang sama dengan `whereColumn` dengan `whereValue`.
            - Untuk baris yang nilainya sama, lanjutkan proses. Skip yang tidak memenuhi syarat
        - Copy baris, gunakan `strtok` dan `strcmp` untuk membagi data menjadi substring per kolom.
        - Untuk baris yang terdapat di `columns`, cetak di `temp`.
        - Tambahkan `temp` ke `sendS` menggunakan `strcat`.
## 5. UPDATE
- Fungsi : Mengganti nilai salah satu kolom dalam semua/sebagian baris tabel.
- Argumen :
    - database : Database lokasi tabel.
    - table : Nama tabel.
    - all : Boolean, apakah diminta semua kolom atau tidak.
    - column : Nama kolom yang diganti datanya.
    - newData : Data baru.
    - isWhere : Boolean, apakah ada syarat WHERE atau tidak.
    - whereColumn : Nama kolom yang dijadikan syarat WHERE.
    - whereValue : Nilai yang dijadikan syarat WHERE.
- Cara kerja :
    - Buat path database, lalu periksa apakah folder tersebut bisa diakses/ada atau tidak menggunakan `opendir`. Keluar jika tidak bisa.
    - Buat path untuk file, lalu periksa apakah file ada atau tidak menggunakan `access`. Keluar jika tidak ditemukan.
    - Baca dan simpan data dalam file menggunakan `fread`. 
    - Copy data kolom di tabel, lalu pakai `strtok` dan `strcmp` untuk mencari index kolom di `column`. Index akan disimpan di dalam `columnNum`.
    - Jika ada kolom di `columns` memiliki tipe data `string`, cek formatting `whereValue`. Jika benar, hapus karakter `'` menggunakan `memmove` untuk menggeser data ke depan (`'data'` -> `data''`) lalu tandai end of line (`data''` -> `data\0''`).
    - JIKA `isWhere == 1` :
        - Copy data kolom di tabel, lalu pakai `strtok` dan `strcmp` untuk mencari index `whereColumn`.
        - Jika `whereColumn` memiliki tipe data `string`, cek formatting `whereValue`. Jika benar, hapus karakter `'` menggunakan `memmove` untuk menggeser data ke depan (`'data'` -> `data''`) lalu tandai end of line (`data''` -> `data\0''`).
    - Siapkan array untuk hasil PER BARIS dari operasi SELECT yaitu `temp`.

    

    - JIKA `isWhere == 1` :

        UNTUK SETIAP BARIS DATA
        - Siapkan variabel `tempRow`.
        - Copy baris, gunakan `strtok` dan `strcmp` untuk membandingkan nilai di kolom yang sama dengan `whereColumn` dengan `whereValue`.
        - Untuk baris yang nilainya sama, gunakan `strtok`, `strcmp`, dan `strcat` untuk menambahkan semua data lama ke `tempRow` kecuali untuk kolom yang sama dengan `column`. Untuk kolom tersebut ganti dengan `newData`.
        - Gunakan `strcpy` untuk mengganti data lama dengan `tempRow`.
    - ELSE :
        - Proses semua bari dengan cara yang sama, yaitu gunakan `strtok`, `strcmp`, dan `strcat` untuk menambahkan semua data lama ke `tempRow` kecuali untuk kolom yang sama dengan `column`. Untuk kolom tersebut ganti dengan `newData`.
        - Gunakan `strcpy` untuk mengganti data lama dengan `tempRow`.
    - Buka file tabel dengan `fopen` dengan mode `wb` agar isi file terhapus. Simpan struct yang diubah ke dalam file tersebut menggunakan `fwrite`.
    - Cek return value `fwrite`.

## 5. DELETE
- Fungsi : Menghapus semua/sebagian baris data tabel.
- Argumen :
    - database : Database lokasi tabel.
    - table : Nama tabel.
    - isWhere : Boolean, apakah ada syarat WHERE atau tidak.
    - whereColumn : Nama kolom yang dijadikan syarat WHERE.
    - whereValue : Nilai yang dijadikan syarat WHERE.
- Cara kerja :
    - Buat path database, lalu periksa apakah folder tersebut bisa diakses/ada atau tidak menggunakan `opendir`. Keluar jika tidak bisa.
    - Buat path untuk file, lalu periksa apakah file ada atau tidak menggunakan `access`. Keluar jika tidak ditemukan.
    - Baca dan simpan data dalam file menggunakan `fread`. 

    - JIKA `isWhere == 1` :
        - Copy data kolom di tabel, lalu pakai `strtok` dan `strcmp` untuk mencari index `whereColumn`.
        - Jika `whereColumn` memiliki tipe data `string`, cek formatting `whereValue`. Jika benar, hapus karakter `'` menggunakan `memmove` untuk menggeser data ke depan (`'data'` -> `data''`) lalu tandai end of line (`data''` -> `data\0''`).

        UNTUK SETIAP BARIS DATA
        - Copy baris, gunakan `strtok` dan `strcmp` untuk membandingkan nilai di kolom yang sama dengan `whereColumn` dengan `whereValue`.
        - Untuk baris yang nilainya sama :
            - Jika index baris yang dihapus adalah **x**, maka untuk semua baris setelahnya (y > x), ganti **x** dengan **x+1**, lalu **x+1** dengan **x+2**, dst menggunakan `sprintf`.
            - Kurangi jumlah row di struct tabel.

    - ELSE :
        - Tandai index pertama setiap baris data dengan `'\0'` agar seolah-olah merupakan string panjang 0.
        - Ubah jumlah row menjadi 0.
    - Buka file tabel dengan `fopen` dengan mode `wb` agar isi file terhapus. Simpan struct yang diubah ke dalam file tersebut menggunakan `fwrite`.
    - Cek return value `fwrite`.

## 6 MAIN (UNTUK BAGIAN SELAIN SETUP SOCKET DAN USER ACCOUNT)
- Jadikan proses sebagai daemon :
    - Simpan working directory proses parent menggunakan `getcwd`.
    - `Fork` untuk membuat child process.
    - `exit` dari parent process
    - Ganti permission dan working directory dari child ke working directory parent awal.


- SETUP SOCKET DAN USER ACCOUNT

- Siapkan semua char array untuk menyimpan potongan instruksi dan argumen instruksi termasuk `input` untuk menyimpan instruksi utuh.
- Inisialisasi :
    - isExit : Sentinel variable untuk `do while` loop yang digunakan database untuk menerima instruksi hingga client exit.
    - curDB : Nama database yang saat ini diakses, inisialisasi awal dengan konstanta `NO_DB`.
    - tokenMain : Pointer untuk menyimpan potongan substring yang dibuat `strtok`.
- do while loop selama `isExit == 1` :
    - Baca perintah dari client menggunakan `read` dan simpan dalam `input` menggunakan `strcpy`.
    - Buat log perintah `input` menggunakan fungsi `logging`.

    - **Pemotongan perintah menjadi substring dan pencocokan menggunakan `strcmp` :**

        - strtok pertama, simpan potongan pertama instruksi sebagai `command1`.
        - NESTED IF ELSE, mencocokkan ke semua kata pertama perintah :
            - JIKA TIDAK ADA MATCH, anggap perintah tidak valid dan `send` pesan error.
            - JIKA MERUPAKAN `CREATE` atau `DROP`, ada potongan command ke-2 untuk tipenya :
                - strtok kedua, simpan potongan instruksi sebagai `command2`. Cocokkan dengan kemungkinan-kemungkinannya. `send` error jika tidak ditemukan.
            - JIKA MERUPAKAN INSTRUKSI VALID LAINNYA, lanjut ke menerima argumen.
        
        Tahap selanjutnya adalah pencocokan kata kunci seperti `FROM` dan menyimpan argumen fungsi di `comArgs` 1-3 (5 untuk UPDATE + WHERE). **AKAN TETAPI** ada beberapa kasus khusus :
        - SELECT (Menyimpan kolom-kolom yang harus dicetak) :
            - `strtok` dan `strcpy` untuk menyimpan nama semua argumen di `args`.
        - Fungsi dengan WHERE (Mendeteksi apakah ada WHERE atau tidak):
            - Mencoba `strtok` lagi setelah mendapatkan nama tabel. Jika `NULL` maka tidak ada WHERE, sebaliknya jika `WHERE` maka ambil nama kolom dan nilai syarat `WHERE`.
        - USE dan DROP DATABASE (Memeriksa **permission** user saat ini kecuali **root**):
            - Panggil fungsi `checkDbPermission` untuk memeriksa apakah user memiliki permission untuk mengakses/menghapus database tersebut. Jika tidak kirim error.

# database.c (BAGIAN SEPUTAR ACCOUNT)

## A. Autentikasi

Untuk autentikasi user perlu memanggil program client dengan format

```
./[program_client_database] -u [username] -p [password]
```

untuk verifikasi program client akan mengirim username dan password ke program database untuk di verifikasi dengan database user.
Fungsi untuk autentikasi adalah

```
int auth(char *username, char *password)
{
    char line[MAXDATALEN];
    char fname[] = "databases/user.csv";
    FILE *fp = fopen(fname, "r");
    if (fp == NULL)
    {
        printf("Error opening file.\n");
        return 0; // Return failure
    }
    while (fgets(line, sizeof(line), fp))
    {
        char savedUsername[MAXDATALEN];
        char savedPassword[MAXDATALEN];

        sscanf(line, "%[^,],%s", savedUsername, savedPassword);

        if (strcmp(username, savedUsername) == 0 && strcmp(password, savedPassword) == 0)
        {
            fclose(fp);
            return 1; // Return success
        }
    }

    fclose(fp);
    return 0; // Return failure
}
```

Database user hanya dapat diakses oleh root. Jika root ingin menambahkan hak akses kepada user maka program client dapat dijalankan dengan superuser

```
sudo ./client_database
```

kemudian menjalankan command

```
CREATE USER [nama_user] IDENTIFIED BY [password_user];
```

maka fungsi berikut akan menyimpan hak akses user tersebut ke database user.

```
void createUser(char *nama, char *password)
{
    struct userStruct user;
    strcpy(user.name, nama);
    strcpy(user.password, password);
    printf("%s,%s\n", user.name, user.password);
    char fname[] = {"databases/user.csv"};
    FILE *fp;
    fp = fopen(fname, "a");
    fprintf(fp, "%s,%s\n", user.name, user.password);
    fclose(fp);
}
```

## B. Autorisasi

Untuk mengakses database, user membutuhkan permission pada database tersebut
menggunakan command

```
USE [nama_database];
```

Root dapat memberikan permission atas database untuk suatu user dengan command

```
GRANT PERMISSION [nama_database] INTO [nama_user];
```

Kemudian fungsi dalam database yang berkerja adalah

```
void addPermission(char *username, char *database)
{
    struct permissionStruct user;
    strcpy(user.name, username);
    strcpy(user.database, database);
    printf("%s %s\n", user.name, user.database);
    char fname[] = {"databases/permission.csv"};
    FILE *fp;
    fp = fopen(fname, "a");
    fprintf(fp, "%s,%s\n", user.name, user.database);
    fclose(fp);
}
```

fungsi tersebut menerima 2 input parameter username dan database yang akan menyimpan hak akses user atas suatu database ke dalam database permission.

## E. Logging

Untuk setiap command yang diinput oleh user maupun root akan disimpan ke dalam suatu file di dalam folder database/log/ dengan format

```
timestamp(yyyy-mm-dd hh:mm:ss):username:command
```

Fungsi yang menjalankan hal tersebut adalah fungsi logging

```
void logging(const char *command, const char *username)
{
    time_t currentTime;
    char timestamp[50];
    currentTime = time(NULL);
    strftime(timestamp, sizeof(timestamp), "%Y-%m-%d %H:%M:%S", localtime(&currentTime));

    const char *logDirectory = "../database/log";

    char logLocation[256];
    sprintf(logLocation, "../database/log/log_%s.txt", username);

    // Create the log directory if not exist
    struct stat st;
    if (stat(logDirectory, &st) == -1)
    {
        system("mkdir ../database/log");
    }

    FILE *fp = fopen(logLocation, "a");
    if (fp == NULL)
    {
        printf("Error: Failed to create log file.\n");
        return;
    }

    fprintf(fp, "%s:%s:%s\n", timestamp, username, command);
    fclose(fp);
}
```

fungsi tersebut menerima 2 input saat dipanggil yaitu command dan username dari user yang memberikan command. setelah itu akan dicatat ke dalam log. setiap user memiliki file log yang berbeda sesuai dengan username yang dimiliki.

# dump.c

```

int cekUser(char name, charpass){
    FILE prak= fopen("../database/databases/user.dat","rb");
    int cari=0;
    struct user u;

    //membuka file database

    while(1){
        fread(&user,sizeof(user),1,prak);
      
        if(strcmp(u.name, name)==0 && strcmp(u.password, pass)==0){

        }
        if(feof(prak)){
            break;
        }
    }
    fclose(prak);

    if(cari==0){
        printf("User Not FOund\n");
        return 0;
    }else{
        return 1;
    }

}

Penjelasan 
        Fungsi cekUser adalah sebuah fungsi dalam bahasa C yang digunakan untuk memeriksa keberadaan pengguna dalam suatu file database. Fungsi ini membuka file database, membaca data pengguna satu per satu, dan memeriksa apakah nama pengguna dan kata sandi yang diberikan cocok dengan data yang ada di dalam file. Jika pengguna ditemukan, fungsi mengembalikan nilai 1; jika tidak ditemukan, fungsi mengembalikan nilai 0. Fungsi ini menggunakan fungsi strcmp untuk membandingkan string dan feof untuk memeriksa akhir file.

```
 if(geteuid() == 0){
   
        ada=1;
    }else{
        int id = geteuid();
     
        ada = cekUser(argv[2],argv[4]);
    }
    if(ada==0){
        return 0;
    }

Penjelasan 
        code di atas di gunakan untuk melakukan pengecekan apakah user tersebut terdapat di database apa tida


```

snprintf(loc, sizeof loc, "../database/log/log%s.log", argv[2]);

 penjelasan 
        snprintf, string "../database/log/log%s.log" akan diformat dengan memasukkan nilai dari argv[2] (argumen kedua yang diberikan saat menjalankan program). Hasil format ini akan disimpan dalam buffer loc.

```

  while (fgets(buff, 20000, file))
  {
    char *token;
    char buff_temp[20000];
    snprintf(buff_temp, sizeof buff_temp, "%s", buff);
    token = strtok(buff_temp, ":");
    int i = 0;

    while (token != NULL)
    {
      strcpy(cmd[i], token);
      i++;
      token = strtok(NULL, ":");
    }

    char *b = strstr(cmd[4], "USE");
    if (b != NULL)
    {
      char *tokens;
      char temp_cmd[20000];
      strcpy(temp_cmd, cmd[4]);
      tokens = strtok(temp_cmd, "; ");
      int j = 0;

      char use_cmd[100][10000];

      while (tokens != NULL)
      {
        strcpy(use_cmd[j], tokens);
        j++;
        tokens = strtok(NULL, "; ");
      }

      char databaseTarget[20000];
      if (strcmp(use_cmd[1], argv[5]) == 0)
       cek = 1;
      else cek = 0;
    }

    if (cek == 1)
      printf("%s", buff);
  }

  fclose(file);
}


Penjelasan :
Kode yang diberikan adalah sebuah loop yang membaca baris-baris teks dari sebuah file. Setiap baris yang dibaca akan diproses dan dicetak jika memenuhi kondisi tertentu. Berikut adalah langkah-langkahnya secara singkat:

    Loop while (fgets(buff, 20000, file)): Loop ini membaca baris-baris teks dari file menggunakan fgets dan menyimpannya dalam buffer buff.

    Pemisahan token: Setiap baris yang dibaca dipisah menjadi token menggunakan strtok dengan pemisah ":". Token-token tersebut disimpan dalam array cmd[].

    Pengecekan perintah "USE": Pada cmd[4], dilakukan pencarian substring "USE" menggunakan strstr. Jika ditemukan, langkah selanjutnya adalah mengambil perintah "USE" dan memisahkannya menjadi token-token dengan menggunakan strtok, dan disimpan dalam array use_cmd[].

    Perbandingan database target: Perintah "USE" dibandingkan dengan argv[5]. Jika keduanya sama, variabel cek diatur menjadi 1; jika tidak, cek diatur menjadi 0.

    Cetak baris yang memenuhi kondisi: Jika nilai cek adalah 1, baris teks asli (buff) dicetak menggunakan printf.

    Penutupan file: Setelah selesai membaca file, file ditutup menggunakan fclose(file)
