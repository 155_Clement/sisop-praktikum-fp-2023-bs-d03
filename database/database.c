#define _XOPEN_SOURCE 500 //Recursive file tree traversal
#include <stdio.h>
#include <sys/socket.h>
#include <time.h>
#include <sys/stat.h> //File related functions
#include <dirent.h>   //Dir related functions
#include <sys/types.h>
#include <errno.h> //Error codes
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <ftw.h> //Recursive file tree traversal
#define PORT 8080

#define MAXDATALEN 200
#define MAXCOLUMN 100

#define DBROOT "databases"
#define NO_DB "NULL"

typedef struct tableStruct
{
    int columnAmount;
    int rowAmount;
    char data[MAXCOLUMN][MAXDATALEN]; // Max 100 column, data max 200
    char type[MAXCOLUMN][10];         // String or int
} tabledata;

struct userStruct
{
    char name[MAXDATALEN];
    char password[MAXDATALEN];
};

struct permissionStruct
{
    char database[MAXDATALEN];
    char name[MAXDATALEN];
};

// Helper functions
static int rmFiles(const char *pathname, const struct stat *sbuf, int type, struct FTW *ftwb)
{
    if (remove(pathname) < 0)
    {
        perror("ERROR: remove");
        return -1;
    }
    return 0;
}


// log Fucntion
void logging(const char *command, const char *username)
{
    time_t currentTime;
    char timestamp[50];
    currentTime = time(NULL);
    strftime(timestamp, sizeof(timestamp), "%Y-%m-%d %H:%M:%S", localtime(&currentTime));

    const char *logDirectory = "../database/log";

    char logLocation[256];
    sprintf(logLocation, "../database/log/log_%s.txt", username);

    // Create the log directory if not exist
    struct stat st;
    if (stat(logDirectory, &st) == -1)
    {
        system("mkdir ../database/log");
    }

    FILE *fp = fopen(logLocation, "a");
    if (fp == NULL)
    {
        printf("Error: Failed to create log file.\n");
        return;
    }

    fprintf(fp, "%s:%s:%s\n", timestamp, username, command);
    fclose(fp);
}

// User account functions

void createUser(char *nama, char *password)
{
    struct userStruct user;
    strcpy(user.name, nama);
    strcpy(user.password, password);
    printf("%s,%s\n", user.name, user.password);
    char fname[] = {"databases/user.csv"};
    FILE *fp;
    fp = fopen(fname, "a");
    fprintf(fp, "%s,%s\n", user.name, user.password);
    fclose(fp);
}

int auth(char *username, char *password)
{
    char line[MAXDATALEN];
    char fname[] = "databases/user.csv";
    FILE *fp = fopen(fname, "r");
    if (fp == NULL)
    {
        printf("Error opening file.\n");
        return 0; // Return failure
    }
    while (fgets(line, sizeof(line), fp))
    {
        char savedUsername[MAXDATALEN];
        char savedPassword[MAXDATALEN];

        sscanf(line, "%[^,],%s", savedUsername, savedPassword);

        if (strcmp(username, savedUsername) == 0 && strcmp(password, savedPassword) == 0)
        {
            fclose(fp);
            return 1; // Return success
        }
    }

    fclose(fp);
    return 0; // Return failure
}

int checkDbPermission(char *username, char *database)
{
    char line[MAXDATALEN];
    char fname[] = "databases/permission.csv";
    FILE *fp = fopen(fname, "r");
    if (fp == NULL)
    {
        printf("Error opening file.\n");
        return 0; // Return failure
    }
    while (fgets(line, sizeof(line), fp))
    {
        char savedUsername[MAXDATALEN];
        char savedDatabase[MAXDATALEN];

        sscanf(line, "%[^,],%s", savedUsername, savedDatabase);

        if (strcmp(username, savedUsername) == 0 && strcmp(database, savedDatabase) == 0)
        {
            fclose(fp);
            return 1; // Return success
        }
    }

    fclose(fp);
    return 0; // Return failure
}

void removeSemicolon(char *str)
{
    int len = strlen(str);
    for (int i = 0; i < len; i++)
    {
        if (str[i] == ';')
        {
            str[i] = '\0';
            break;
        }
    }
}

void addPermission(char *username, char *database)
{
    struct permissionStruct user;
    strcpy(user.name, username);
    strcpy(user.database, database);
    printf("%s %s\n", user.name, user.database);
    char fname[] = {"databases/permission.csv"};
    FILE *fp;
    fp = fopen(fname, "a");
    fprintf(fp, "%s,%s\n", user.name, user.database);
    fclose(fp);
}

int checkUsernameExists(const char *username)
{
    char line[MAXDATALEN];
    char csvFile[] = "databases/user.csv";

    FILE *fp = fopen(csvFile, "r");
    if (fp == NULL)
    {
        printf("Error opening file.\n");
        return 0;
    }
    while (fgets(line, sizeof(line), fp))
    {
        char savedUsername[MAXDATALEN];
        sscanf(line, "%[^,]", savedUsername);
        if (strcmp(username, savedUsername) == 0)
        {
            fclose(fp);
            return 1;
        }
    }

    fclose(fp);
    return 0;
}
// Return :
/*
-1 = ERROR, check errno
0 = Success
*/
// Database and Table functions
int db_create(char *database)
{
    char path[200];
    sprintf(path, "./%s/%s", DBROOT, database);
    return mkdir(path, S_IRWXU | S_IRGRP | S_IXGRP);
}

int table_create(char *database, char *table, char *columns)
{

    char path[200];
    sprintf(path, "./%s/%s", DBROOT, database);

    DIR *dir = opendir(path);
    if (dir)
    {
        closedir(dir);
    }
    else if (ENOENT == errno)
    {
        printf("\nERROR : DATABASE DOESN'T EXIST\n");
        return -2;
    }
    sprintf(path, "./%s/%s/%s", DBROOT, database, table);
    // Check for existance
    if (access(path, F_OK) == 0)
    {
        printf("\nERROR : TABLE ALREADY EXISTS\n");
        return -1;
    }
    else
    {
        columns[0] = ' '; // Erase both brackets
        columns[strlen(columns) - 1] = ' ';

        // STRTOK, get data
        tabledata newEntry;
        newEntry.columnAmount = 0;
        newEntry.rowAmount = 0;
        char *token = strtok(columns, ", ");
        while (token != NULL)
        {
            if (strcmp(token, ";\n") == 0)
                break;
            strcat(newEntry.data[0], token);
            strcat(newEntry.data[0], ";");
            token = strtok(NULL, ", )");
            newEntry.columnAmount++;
            if (token == NULL)
                break;
            strcpy(newEntry.type[newEntry.columnAmount - 1], token);
            token = strtok(NULL, ", ");
        }
        // WRITING TO FILE
        FILE *file = fopen(path, "wb");
        int flag = 0;
        if (file == NULL)
        {
            printf("\nERROR : Failed to create table file\n");
            return -1;
        }

        flag = fwrite(&newEntry, sizeof(tabledata), 1, file);

        fclose(file);

        //'0' if success, '-1' if error
        if (flag)
            flag = 0;
        else
            flag = -1;

        return flag;
    }
}

int db_drop(char *database)
{
    database[strlen(database) - 1] = '\0';
    char path[200];
    sprintf(path, "./%s/%s", DBROOT, database);

    DIR *dir = opendir(path);
    if (dir)
    {
        closedir(dir);
    }
    else if (ENOENT == errno)
    {
        printf("\nERROR : DATABASE DOESN'T EXIST\n");
        return -2;
    }

    if (nftw(path, rmFiles, 2, FTW_DEPTH | FTW_MOUNT | FTW_PHYS) < 0)
    {
        perror("ERROR: ntfw");
        return -1;
    }
    else{
        return 0;
    }
}

int db_use(char *oldDB, char *newDB)
{
    char path[200];
    sprintf(path, "./%s/%s", DBROOT, newDB);

    DIR *dir = opendir(path);
    if (dir)
    {
        closedir(dir);
    }
    else if (ENOENT == errno)
    {
        printf("\nERROR : DATABASE DOESN'T EXIST\n");
        return -2;
    }

    strcpy(oldDB, newDB);
    return 0;
}

int table_drop(char *database, char *table)
{
    char path[200];
    sprintf(path, "./%s/%s", DBROOT, database);

    DIR *dir = opendir(path);
    if (dir)
    {
        closedir(dir);
    }
    else if (ENOENT == errno)
    {
        printf("\nERROR : DATABASE DOESN'T EXIST\n");
        return -2;
    }
    sprintf(path, "./%s/%s/%s", DBROOT, database, table);
    // Check for existance
    if (access(path, F_OK) == 0)
    {
        if (remove(path) != 0)
        {
            printf("\nERROR DURING DELETION\n");
            return -1;
        }
    }
    else
    {
        printf("\nERROR : TABLE DOESN'T EXISTS\n");
        return -1;
    }
    return 0;
}

int dml_dropColumn(char *database, char *table, char *column)
{
    char path[200];
    sprintf(path, "./%s/%s", DBROOT, database);

    DIR *dir = opendir(path);
    if (dir)
    {
        closedir(dir);
    }
    else if (ENOENT == errno)
    {
        printf("\nERROR : DATABASE DOESN'T EXIST\n");
        return -2;
    }
    sprintf(path, "./%s/%s/%s", DBROOT, database, table);
    // Check for existance
    if (access(path, F_OK) == 0)
    {
        // Get table data
        FILE *file = fopen(path, "rb+");
        int flag = 0;
        if (file == NULL)
        {
            printf("\nERROR : Failed to open table file\n");
            return -1;
        }

        tabledata data;
        fread(&data, sizeof(tabledata), 1, file);
        fclose(file);

        // STRTOK to add new row
        int columnNum = 0;
        int columnCount = 0;
        char columnCopy[MAXDATALEN];
        char temp[1000];
        sprintf(columnCopy, "%s", data.data[0]);
        columnNum = 0;
        char *selectToken = strtok(columnCopy, ";");
        while (selectToken != NULL)
        {
            if (strcmp(selectToken, column) == 0)
            {
                break;
            }
            selectToken = strtok(NULL, ";");
            columnNum++;
        }

        for (int x = 0; x <= data.rowAmount; x++)
        {
            char tempRow[MAXDATALEN];
            tempRow[0] = '\0';
            sprintf(columnCopy, "%s", data.data[x]);
            columnCount = 0;
            char *selectToken = strtok(columnCopy, ";");
            while (selectToken != NULL)
            {
                if (columnCount == columnNum)
                {
                    continue;
                }
                else
                {
                    strcat(tempRow, selectToken);
                }
                strcat(tempRow, ";");
                selectToken = strtok(NULL, ";");
                columnCount++;
            }
            strcpy(data.data[x], tempRow);
        }

        // WRITING TO FILE

        flag = 0;
        file = fopen(path, "wb");
        if (file == NULL)
        {
            printf("\nERROR : Failed to write to table file\n");
            return -1;
        }

        flag = fwrite(&data, sizeof(tabledata), 1, file);

        fclose(file);

        //'0' if success, '-1' if error
        if (flag)
            flag = 0;
        else
            flag = -1;

        return flag;
    }
    else
    {
        printf("\nERROR : TABLE DOESN'T EXISTS\n");
        return -1;
    }
}

int dml_insert(char *database, char *table, char *columns)
{
    char path[200];
    sprintf(path, "./%s/%s", DBROOT, database);

    DIR *dir = opendir(path);
    if (dir)
    {
        closedir(dir);
    }
    else if (ENOENT == errno)
    {
        printf("\nERROR : DATABASE DOESN'T EXIST\n");
        return -2;
    }
    sprintf(path, "./%s/%s/%s", DBROOT, database, table);
    // Check for existance
    if (access(path, F_OK) == 0)
    {
        // Get table data
        FILE *file = fopen(path, "rb+");
        int flag = 0;
        if (file == NULL)
        {
            printf("\nERROR : Failed to open table file\n");
            return -1;
        }

        tabledata data;
        fread(&data, sizeof(tabledata), 1, file);
        fclose(file);

        columns[0] = ' '; // Erase both brackets
        columns[strlen(columns) - 1] = ' ';
        // STRTOK to add new row
        data.rowAmount++;
        int countType = 0;

        char *token = strtok(columns, ", ");
        char temp[MAXDATALEN];
        while (token != NULL)
        {
            if (strcmp(token, ";\n") == 0)
                break;
            if (strcmp(data.type[countType], "string") == 0)
            {
                sprintf(temp, "%s", token);
                if (temp[0] == '\'')
                {
                    if (temp[strlen(temp) - 1] == '\'')
                    {
                        memmove(temp, temp + 1, strlen(temp) - sizeof * temp);
                        temp[strlen(temp) - 2] = '\0';
                        strcat(data.data[data.rowAmount], temp);
                        strcat(data.data[data.rowAmount], ";");
                        countType++;
                    }
                    else
                    {
                        data.rowAmount--;
                        printf("ERROR : STRING FORMATTING\n");
                      return -1;
                    }
                }
                else
                {
                    data.rowAmount--;
                    printf("ERROR : STRING FORMATTING\n");
                    return -1;
                }
            }
            else
            {
                strcat(data.data[data.rowAmount], token);
                strcat(data.data[data.rowAmount], ";");
                countType++;
            }

            token = strtok(NULL, ", ");
        }
        // WRITING TO FILE

        flag = 0;
        file = fopen(path, "wb");
        if (file == NULL)
        {
            printf("\nERROR : Failed to write to table file\n");
            return -1;
        }

        flag = fwrite(&data, sizeof(tabledata), 1, file);

        fclose(file);

        //'0' if success, '-1' if error
        if (flag)
            flag = 0;
        else
            flag = -1;

        return flag;
    }
    else
    {
        printf("\nERROR : TABLE DOESN'T EXISTS\n");
        return -1;
    }
}

int dml_select(char *database, char *table, int all, char columns[][MAXDATALEN], int isWhere, char *whereColumn, char *whereValue, char *sendS)
{
    char path[200];
    sprintf(path, "./%s/%s", DBROOT, database);

    DIR *dir = opendir(path);
    if (dir)
    {
        closedir(dir);
    }
    else if (ENOENT == errno)
    {
        sprintf(sendS, "\nERROR : DATABASE DOESN'T EXIST\n");
        return -2;
    }
    sprintf(path, "./%s/%s/%s", DBROOT, database, table);
    // Check for existance
    if (access(path, F_OK) == 0)
    {
        // Get table data
        FILE *file = fopen(path, "rb+");
        int flag = 0;
        if (file == NULL)
        {
            sprintf(sendS, "\nERROR : Failed to open table file\n");
            return -1;
        }

        tabledata data;
        fread(&data, sizeof(tabledata), 1, file);
        fclose(file);

        int whereColumnCount = 0;

        int whereFound = 0;

        int whereColumnNum = 0;

        char columnCopy[MAXDATALEN];
        // WHERE
        if (isWhere)
        {

            sprintf(columnCopy, "%s", data.data[0]);
            char *selectToken = strtok(columnCopy, ";");
            // Finds column position for WHERE
            while (selectToken != NULL)
            {
                if (strcmp(selectToken, whereColumn) == 0)
                {
                    whereFound = 1;
                }
                selectToken = strtok(NULL, ";");
                if (!whereFound)
                    whereColumnNum++;
            }

            // Reformat string for WHERE
            if (strcmp(data.type[whereColumnNum], "string") == 0)
            {
                if (whereValue[0] == '\'')
                {
                    if (whereValue[strlen(whereValue) - 1] == '\'')
                    {
                        memmove(whereValue, whereValue + 1, strlen(whereValue)  - sizeof * whereValue);
                        whereValue[strlen(whereValue) - 2] = '\0';
                    }
                    else
                    {
                        printf("ERROR : STRING FORMATTING\n");
                        return -1;
                    }
                }
                else
                {
                    printf("ERROR : STRING FORMATTING\n");
                    return -1;
                }
            }
        }
        char temp[1000];
        if (all == 1)
        {
            // Column headers
            sprintf(temp, "%d : %s\n", 0, data.data[0]);
            strcat(sendS, temp);
            // Column contents
            for (int x = 1; x <= data.rowAmount; x++)
            {
                if (isWhere)
                {
                    char tempRow[MAXDATALEN];
                    tempRow[0] = '\0';
                    sprintf(columnCopy, "%s", data.data[x]);
                    int columnCount = 0;
                    char *selectToken = strtok(columnCopy, ";");
                    while (selectToken != NULL)
                    {
                        if (columnCount == whereColumnCount)
                        {
                            break;
                        }
                        else
                        {
                            strcat(tempRow, selectToken);
                        }
                        strcat(tempRow, ";");
                        selectToken = strtok(NULL, ";");
                        columnCount++;
                    }
                    if (strcmp(selectToken, whereValue) != 0)
                    {
                        continue;
                    }
                    else
                    {
                        sprintf(temp, "%d : %s\n", x, data.data[x]);
                        strcat(sendS, temp);
                    }
                }
                else
                {
                    sprintf(temp, "%d : %s\n", x, data.data[x]);
                    strcat(sendS, temp);
                }
            }
        }

        else
        {
            int attrNum = 0;
            int columnNum = 0;
            char columnCopy[MAXDATALEN];
            char temp[1000];
            while (columns[attrNum][0] != '\0')
            {
                sprintf(columnCopy, "%s", data.data[0]);
                columnNum = 0;
                char *selectToken = strtok(columnCopy, ";");
                while (selectToken != NULL)
                {
                    if (strcmp(selectToken, columns[attrNum]) == 0)
                    {
                        sprintf(temp, "%d", columnNum);
                        strcpy(columns[attrNum], temp);
                        break;
                    }
                    selectToken = strtok(NULL, ";");
                    columnNum++;
                }
                attrNum++;
            }

            for (int x = 1; x <= data.rowAmount; x++)
            {
                // Checks for where validity
                if (isWhere)
                {
                    char tempRow[MAXDATALEN];
                    tempRow[0] = '\0';
                    sprintf(columnCopy, "%s", data.data[x]);
                    int columnCount = 0;
                    char *selectToken = strtok(columnCopy, ";");
                    while (selectToken != NULL)
                    {
                        if (columnCount == whereColumnCount)
                        {
                            break;
                        }
                        else
                        {
                            strcat(tempRow, selectToken);
                        }
                        strcat(tempRow, ";");
                        selectToken = strtok(NULL, ";");
                        columnCount++;
                    }
                    if (strcmp(selectToken, whereValue) != 0)
                    {
                        continue;
                    }
                }
                // Checks column to print
                sprintf(temp, "%d : ", x);
                strcat(sendS, temp);
                attrNum = 0;
                while (columns[attrNum][0] != '\0')
                {

                    sprintf(columnCopy, "%s", data.data[x]);
                    columnNum = 0;
                    char *selectToken = strtok(columnCopy, ";");
                    while (selectToken != NULL)
                    {
                        if (columnNum == (columns[attrNum][0] - '0'))
                        {
                            sprintf(temp, "%s;", selectToken);

                            strcat(sendS, temp);
                            break;
                        }
                        selectToken = strtok(NULL, ";");
                        columnNum++;
                    }
                    attrNum++;
                }
                printf("\n");
                strcat(sendS, "\n");
            }
        }
        return 0;
    }
    else
    {
        sprintf(sendS, "TABLE DOESN'T EXISTS\n");
        return -1;
    }
}

int dml_update(char *database, char *table, char *column, char *newData, int isWhere, char *whereColumn, char *whereValue)
{
    char path[200];
    sprintf(path, "./%s/%s", DBROOT, database);

    DIR *dir = opendir(path);
    if (dir)
    {
        closedir(dir);
    }
    else if (ENOENT == errno)
    {
        printf("\nERROR : DATABASE DOESN'T EXIST\n");
        return -2;
    }
    sprintf(path, "./%s/%s/%s", DBROOT, database, table);
    // Check for existance
    if (access(path, F_OK) == 0)
    {
        // Get table data
        FILE *file = fopen(path, "rb+");
        int flag = 0;
        if (file == NULL)
        {
            printf("\nERROR : Failed to open table file\n");
            return -1;
        }

        tabledata data;
        fread(&data, sizeof(tabledata), 1, file);
        fclose(file);

        // Used for checking columns (INSERT)

        int columnCount = 0;
        int columnFound = 0;
        int columnNum = 0;

        char columnCopy[MAXDATALEN];
        char temp[1000];
        sprintf(columnCopy, "%s", data.data[0]);
        columnNum = 0;
        char *selectToken = strtok(columnCopy, ";");
        // Finds column position for INSERT
        while (selectToken != NULL)
        {
            if (strcmp(selectToken, column) == 0)
            {
                columnFound = 1;
            }

            selectToken = strtok(NULL, ";");
            if (!columnFound)
                columnNum++;
        }
        // Reformat string for INSERT

        if (strcmp(data.type[columnNum], "string") == 0)
        {
            if (newData[0] == '\'')
            {
                if (newData[strlen(newData) - 1] == '\'')
                {

                    memmove(newData, newData + 1, strlen(newData) - sizeof * newData);
                    newData[strlen(newData) - 2] = '\0';
                }
                else
                {
                    printf("ERROR : STRING FORMATTING\n");
                    return -1;
                }
            }
            else
            {
                printf("ERROR : STRING FORMATTING\n");
                return -1;
            }
        }


        // WHERE
        int whereColumnCount = 0;

        int whereFound = 0;

        int whereColumnNum = 0;
        if (isWhere)
        {
            sprintf(columnCopy, "%s", data.data[0]);
            char *selectToken = strtok(columnCopy, ";");
            // Finds column position for WHERE
            while (selectToken != NULL)
            {
                if (strcmp(selectToken, whereColumn) == 0)
                {
                    whereFound = 1;
                }
                selectToken = strtok(NULL, ";");
                if (!whereFound)
                    whereColumnNum++;
            }

            // Reformat string for WHERE
            if (strcmp(data.type[whereColumnNum], "string") == 0)
            {
                if (whereValue[0] == '\'')
                {
                    if (whereValue[strlen(whereValue) - 1] == '\'')
                    {
                        memmove(whereValue, whereValue + 1, strlen(whereValue)  - sizeof * whereValue);
                        whereValue[strlen(whereValue) - 2] = '\0';
                    }
                    else
                    {
                        printf("ERROR : STRING FORMATTING\n");
                        return -1;
                    }
                }
                else
                {
                    printf("ERROR : STRING FORMATTING\n");
                    return -1;
                }
            }
        }

        if (isWhere)
        {
            for (int x = 1; x <= data.rowAmount; x++)
            {
                if (isWhere)
                {

                    sprintf(columnCopy, "%s", data.data[x]);
                    int columnCount = 0;
                    char *selectToken = strtok(columnCopy, ";");
                    while (selectToken != NULL)
                    {
                        if (columnCount == whereColumnCount)
                        {
                            break;
                        }

                        selectToken = strtok(NULL, ";");
                        columnCount++;
                    }
                    if (strcmp(selectToken, whereValue) != 0)
                    {
                        continue;
                    }
                    else
                    {
                        char tempRow[MAXDATALEN];
                        tempRow[0] = '\0';
                        sprintf(columnCopy, "%s", data.data[x]);
                        columnCount = 0;
                        whereColumnCount = 0;
                        char *selectToken = strtok(columnCopy, ";");
                        while (selectToken != NULL)
                        {
                            if (columnCount == columnNum)
                            {
                                strcat(tempRow, newData);
                            }
                            else
                            {
                                strcat(tempRow, selectToken);
                            }
                            strcat(tempRow, ";");
                            selectToken = strtok(NULL, ";");
                            columnCount++;
                        }

                        strcpy(data.data[x], tempRow);
                    }
                }
            }
        }
        else
        {
            for (int x = 1; x <= data.rowAmount; x++)
            {
                char tempRow[MAXDATALEN];
                tempRow[0] = '\0';
                sprintf(columnCopy, "%s", data.data[x]);
                columnCount = 0;
                whereColumnCount = 0;
                char *selectToken = strtok(columnCopy, ";");
                while (selectToken != NULL)
                {
                    if (columnCount == columnNum)
                    {
                        strcat(tempRow, newData);
                    }
                    else
                    {
                        strcat(tempRow, selectToken);
                    }
                    strcat(tempRow, ";");
                    selectToken = strtok(NULL, ";");
                    columnCount++;
                }

                strcpy(data.data[x], tempRow);
            }
        }
        // WRITING TO FILE

        flag = 0;
        file = fopen(path, "wb");
        if (file == NULL)
        {
            printf("\nERROR : Failed to write to table file\n");
            return -1;
        }

        flag = fwrite(&data, sizeof(tabledata), 1, file);

        fclose(file);

        //'0' if success, '-1' if error
        if (flag)
            flag = 0;
        else
            flag = -1;

        return flag;
    }
    else
    {
        printf("\nERROR : TABLE DOESN'T EXISTS\n");
        return -1;
    }
}

int dml_delete(char *database, char *table, int isWhere, char *whereColumn, char *whereValue)
{
    char path[200];
    sprintf(path, "./%s/%s", DBROOT, database);

    DIR *dir = opendir(path);
    if (dir)
    {
        closedir(dir);
    }
    else if (ENOENT == errno)
    {
        printf("\nERROR : DATABASE DOESN'T EXIST\n");
        return -2;
    }
    sprintf(path, "./%s/%s/%s", DBROOT, database, table);
    // Check for existance
    if (access(path, F_OK) == 0)
    {
        // Get table data
        FILE *file = fopen(path, "rb+");
        int flag = 0;
        if (file == NULL)
        {
            printf("\nERROR : Failed to open table file\n");
            return -1;
        }

        tabledata data;
        fread(&data, sizeof(tabledata), 1, file);
        fclose(file);

        if (isWhere)
        {

            int whereColumnCount = 0;

            int whereFound = 0;

            int whereColumnNum = 0;

            char columnCopy[MAXDATALEN];
            // WHERE
            if (isWhere)
            {

                sprintf(columnCopy, "%s", data.data[0]);
                char *selectToken = strtok(columnCopy, ";");
                // Finds column position for WHERE
                while (selectToken != NULL)
                {
                    if (strcmp(selectToken, whereColumn) == 0)
                    {
                        whereFound = 1;
                    }
                    selectToken = strtok(NULL, ";");
                    if (!whereFound)
                        whereColumnNum++;
                }

                // Reformat string for WHERE
                if (strcmp(data.type[whereColumnNum], "string") == 0)
                {
                    if (whereValue[0] == '\'')
                    {
                        if (whereValue[strlen(whereValue) - 1] == '\'')
                        {
                            memmove(whereValue, whereValue + 1, strlen(whereValue)  - sizeof * whereValue);
                            whereValue[strlen(whereValue) - 2] = '\0';
                        }
                        else
                        {
                            printf("ERROR : STRING FORMATTING\n");
                            return -1;
                        }
                    }
                    else
                    {
                        printf("ERROR : STRING FORMATTING\n");
                        return -1;
                    }
                }
            }

            for (int x = 1; x <= data.rowAmount; x++)
            {
                if (isWhere)
                {

                    sprintf(columnCopy, "%s", data.data[x]);
                    int columnCount = 0;
                    char *selectToken = strtok(columnCopy, ";");
                    while (selectToken != NULL)
                    {
                        if (columnCount == whereColumnCount)
                        {
                            break;
                        }

                        selectToken = strtok(NULL, ";");
                        columnCount++;
                    }
                    if (strcmp(selectToken, whereValue) != 0)
                    {
                        continue;
                    }
                    else
                    {
                        data.data[x][0] = '\0';
                        for (int y = x+1; y <= data.rowAmount; y++){
                            int prev = y-1;
                            int next = y;
                            sprintf(data.data[prev], "%s", data.data[next]);
                            
                        }
                        data.rowAmount--;
                    }
                }
            }
        }
        else
        {
            for (int x = 1; x <= data.rowAmount; x++)
            {
                data.data[x][0] = '\0';
            }
            data.rowAmount = 0;
        }

        // WRITING TO FILE

        flag = 0;
        file = fopen(path, "wb");
        if (file == NULL)
        {
            printf("\nERROR : Failed to write to table file\n");
            return -1;
        }

        flag = fwrite(&data, sizeof(tabledata), 1, file);

        fclose(file);

        //'0' if success, '-1' if error
        if (flag)
            flag = 0;
        else
            flag = -1;

        return flag;
    }
    else
    {
        printf("\nERROR : TABLE DOESN'T EXISTS\n");
        return -1;
    }
}

int main(int argc, char const *argv[])
{
    //SETUP DAEMON
    char workDir[MAXDATALEN];
    getcwd(workDir, sizeof(workDir));

    pid_t pid, sid;        // Variabel untuk menyimpan PID

    pid = fork();     // Menyimpan PID dari Child Process

    /* Keluar saat fork gagal
    * (nilai variabel pid < 0) */
    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    /* Keluar saat fork berhasil
    * (nilai variabel pid adalah PID dari child process) */
   
    if (pid > 0) {
        printf("PID : %d || %sn\n", pid, workDir);
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    if ((chdir(workDir)) < 0) {
        
        exit(EXIT_FAILURE);
    }

    //close(STDIN_FILENO);
    //close(STDOUT_FILENO);
    //close(STDERR_FILENO);
    


    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};
    char *hello = "Hello from server";

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    {
        printf("HAHAHA");
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)))
    {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0)
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0)
    {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0)
    {
        perror("accept");
        exit(EXIT_FAILURE);
    }
    
    char sendS[1024];
    int sudo;
    valread = read(new_socket, &sudo, sizeof(sudo));
    int sudof = ntohl(sudo);
    printf("SUDO: %d\n", sudof);
    strcpy(sendS, "Sudo Confirmed");
    send(new_socket, sendS, strlen(sendS), 0);
    memset(sendS, 0, sizeof(sendS));

    /*WRITE CODE BODY HERE*/
    mkdir(DBROOT, S_IRWXU | S_IRGRP | S_IXGRP);
    /*Single user checking*/
    char username[1024]; // Used for username checking on query while keeping pass secret
    char password[1024];
    if (sudof == 1)
    {
        // if sudo username = root (for logging);
        strcpy(username, "root");
    }
    else if (sudof == 0)
    {

        
        // receive username
        valread = read(new_socket, buffer, 1024);
        strcpy(username, buffer);
        printf("Username: %s\n", username);
        strcpy(sendS, "username received");
        send(new_socket, sendS, strlen(sendS), 0);
        memset(buffer, 0, sizeof(buffer));

        // receive password
        valread = read(new_socket, buffer, 1024);
        strcpy(password, buffer);
        printf("password: %s\n", password);
        memset(buffer, 0, sizeof(buffer));

        int authentication = auth(username, password);
        if (authentication == 1)
        {
            strcpy(sendS, "SERVER: Authentication Success");
            send(new_socket, sendS, strlen(sendS), 0);
        }
        else if (authentication == 0)
        {
            strcpy(sendS, "FAILED");
            send(new_socket, sendS, strlen(sendS), 0);
            printf("Gagal Auth\n");
            return 0;
        }
    }

    /*Command input*/
    // ERROR CHECK : Command ended with ';'??
    char input[MAXDATALEN * 3];
    // char input2[MAXDATALEN*3];
    char command1[100];
    char command2[100];
    char comArgs1[300];
    char comArgs2[300];
    char comArgs3[300];
    char where[100];
    char whereArgs[200];
    char trash[100];
    int isExit = 1;
    char curDB[100]; // Working database
    char *tokenMain;
    // DEBUG
    strcpy(curDB, NO_DB);

    do
    {
        printf("PLEASE INPUT COMMAND :\n> ");

        valread = read(new_socket, buffer, 1024);
        strcpy(input, buffer);
        printf("INPUT: %s\n", input);
        memset(buffer, 0, sizeof(buffer));
        //Create log
        logging(input, username);

        tokenMain = strtok(input, " ");
        sprintf(command1, "%s", tokenMain);

        // TODO : MUST BE UPPERCASE
        if (strcmp(command1, "CREATE") != 0)
        {
            if (strcmp(command1, "USE") != 0)
            {
                if (strcmp(command1, "GRANT") != 0)
                {
                    if (strcmp(command1, "SELECT") != 0)
                    {
                        if (strcmp(command1, "INSERT") != 0)
                        {
                            if (strcmp(command1, "UPDATE") != 0)
                            {
                                if (strcmp(command1, "DROP") != 0)
                                {
                                    if (strcmp(command1, "DELETE") != 0)
                                    {
                                        if (strcmp(command1, "EXIT;") != 0)
                                        {
                                            printf("ERROR : UNKNOWN COMMAND\n");
                                            send(new_socket, "ERROR : UNKNOWN COMMAND\n", strlen("ERROR : UNKNOWN COMMAND\n"), 0);
                                            continue;
                                        }
                                        else
                                        {
                                            send(new_socket, "EXIT", strlen("EXIT"), 0);
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        tokenMain = strtok(NULL, " ");
                                        if (tokenMain == NULL)
                                        {
                                            printf("ERROR : ARGS");
                                            send(new_socket, "ERROR : FORMAT\n", strlen("ERROR : FORMAT\n"), 0);
                                            continue;
                                        }
                                        // ERROR CHECKING
                                        tokenMain = strtok(NULL, " ");
                                        if (tokenMain == NULL)
                                        {
                                            printf("ERROR : ARGS");
                                            send(new_socket, "ERROR : FORMAT\n", strlen("ERROR : FORMAT\n"), 0);
                                            continue;
                                        }
                                        sprintf(comArgs1, "%s", tokenMain);
                                        // WHERE DETECT START
                                        int isWhere = 0;
                                        tokenMain = strtok(NULL, " ");

                                        // If NO WHERE
                                        if (tokenMain == NULL)
                                        {
                                            comArgs1[strlen(comArgs1) - 1] = '\0';
                                        }
                                        // If THERE's WHERE
                                        else if (strcmp(tokenMain, "WHERE") == 0)
                                        {
                                            // Column
                                            isWhere = 1;
                                            tokenMain = strtok(NULL, "=");
                                            if (tokenMain == NULL)
                                            {
                                                printf("ERROR : ARGS");
                                                send(new_socket, "ERROR : FORMAT\n", strlen("ERROR : FORMAT\n"), 0);
                                                continue;
                                            }
                                            // ERROR CHECKING
                                            sprintf(comArgs2, "%s", tokenMain);
                                            tokenMain = strtok(NULL, ";");
                                            if (tokenMain == NULL)
                                            {
                                                printf("ERROR : ARGS");
                                                send(new_socket, "ERROR : FORMAT\n", strlen("ERROR : FORMAT\n"), 0);
                                                continue;
                                            }
                                            // ERROR CHECKING
                                            sprintf(comArgs3, "%s", tokenMain);
                                        }
                                        // WHERE DETECT END
                                        if (dml_delete(curDB, comArgs1, isWhere, comArgs2, comArgs3) == 0)
                                        {
                                            send(new_socket, "SUCCESS\n", strlen("SUCCESS\n"), 0);
                                        }
                                        else
                                        {
                                            send(new_socket, "ERROR EXEC\n", strlen("ERROR EXEC\n"), 0);
                                        }
                                    }
                                }
                                else
                                {
                                    tokenMain = strtok(NULL, " ");
                                    if (tokenMain == NULL)
                                    {
                                        printf("ERROR : ARGS");
                                        send(new_socket, "ERROR : FORMAT\n", strlen("ERROR : FORMAT\n"), 0);
                                        continue;
                                    }
                                    sprintf(command2, "%s", tokenMain);
                                    if (strcmp(command2, "COLUMN") != 0)
                                    {
                                        if (strcmp(command2, "TABLE") != 0)
                                        {
                                            if (strcmp(command2, "DATABASE") != 0)
                                            {
                                                printf("ERROR : UNKNOWN DROP COMMAND\n");
                                                send(new_socket, "ERROR : FORMAT\n", strlen("ERROR : FORMAT\n"), 0);
                                                continue;
                                            }
                                            else
                                            {
                                                tokenMain = strtok(NULL, " ");
                                                if (tokenMain == NULL)
                                                {
                                                    printf("ERROR : ARGS");
                                                    send(new_socket, "ERROR : FORMAT\n", strlen("ERROR : FORMAT\n"), 0);
                                                    continue;
                                                }
                                                sprintf(comArgs1, "%s", tokenMain);
                                                // Permission Checking
                                                if (sudof)
                                                    ;
                                                else if (!checkDbPermission(username, comArgs1))
                                                {
                                                    printf("ERROR : DB ACCESS DENIED");
                                                    send(new_socket, "ERROR : DB ACCESS DENIED\n", strlen("ERROR : DB ACCESS DENIED\n"), 0);
                                                    continue;
                                                }
                                                if (db_drop(comArgs1) == 0)
                                                {
                                                    send(new_socket, "SUCCESS\n", strlen("SUCCESS\n"), 0);
                                                }
                                                else
                                                {
                                                    send(new_socket, "ERROR EXEC\n", strlen("ERROR EXEC\n"), 0);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            tokenMain = strtok(NULL, ";");
                                            if (tokenMain == NULL)
                                            {
                                                printf("ERROR : ARGS");
                                                send(new_socket, "ERROR : FORMAT\n", strlen("ERROR : FORMAT\n"), 0);
                                                continue;
                                            }
                                            sprintf(comArgs1, "%s", tokenMain);
                                            if (table_drop(curDB, comArgs1) == 0)
                                            {
                                                send(new_socket, "SUCCESS\n", strlen("SUCCESS\n"), 0);
                                            }
                                            else
                                            {
                                                send(new_socket, "ERROR EXEC\n", strlen("ERROR EXEC\n"), 0);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        tokenMain = strtok(NULL, " ");
                                        if (tokenMain == NULL)
                                        {
                                            printf("ERROR : ARGS");
                                            send(new_socket, "ERROR : FORMAT\n", strlen("ERROR : FORMAT\n"), 0);
                                            continue;
                                        }
                                        sprintf(comArgs1, "%s", tokenMain);
                                        // FROM
                                        tokenMain = strtok(NULL, " ");
                                        if (tokenMain == NULL)
                                        {
                                            printf("ERROR : ARGS");
                                            send(new_socket, "ERROR : FORMAT\n", strlen("ERROR : FORMAT\n"), 0);
                                            continue;
                                        }
                                        // TABLE
                                        tokenMain = strtok(NULL, ";");
                                        if (tokenMain == NULL)
                                        {
                                            printf("ERROR : ARGS");
                                            send(new_socket, "ERROR : FORMAT\n", strlen("ERROR : FORMAT\n"), 0);
                                            continue;
                                        }
                                        sprintf(comArgs2, "%s", tokenMain);

                                        if (dml_dropColumn(curDB, comArgs2, comArgs1) == 0)
                                        {
                                            // SUCCESS
                                            send(new_socket, "SUCCESS\n", strlen("SUCCESS\n"), 0);
                                        }
                                        // ERROR
                                        else
                                        {
                                            send(new_socket, "ERROR EXEC\n", strlen("ERROR EXEC\n"), 0);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                tokenMain = strtok(NULL, " ");
                                if (tokenMain == NULL)
                                {
                                    printf("ERROR : ARGS");
                                    send(new_socket, "ERROR : FORMAT\n", strlen("ERROR : FORMAT\n"), 0);
                                    continue;
                                }
                                // ERROR CHECKING
                                sprintf(comArgs1, "%s", tokenMain);
                                //"SET"
                                tokenMain = strtok(NULL, " ");
                                if (tokenMain == NULL)
                                {
                                    printf("ERROR : ARGS");
                                    send(new_socket, "ERROR : FORMAT\n", strlen("ERROR : FORMAT\n"), 0);
                                    continue;
                                }
                                // Column
                                tokenMain = strtok(NULL, "=");
                                if (tokenMain == NULL)
                                {
                                    printf("ERROR : ARGS");
                                    send(new_socket, "ERROR : FORMAT\n", strlen("ERROR : FORMAT\n"), 0);
                                    continue;
                                }
                                // ERROR CHECKING
                                sprintf(comArgs2, "%s", tokenMain);
                                tokenMain = strtok(NULL, " ");
                                if (tokenMain == NULL)
                                {
                                    printf("ERROR : ARGS");
                                    send(new_socket, "ERROR : FORMAT\n", strlen("ERROR : FORMAT\n"), 0);
                                    continue;
                                }
                                // ERROR CHECKING
                                sprintf(comArgs3, "%s", tokenMain);

                                // WHERE DETECT START
                                int isWhere = 0;
                                char comArgs4[300];
                                char comArgs5[300];
                                tokenMain = strtok(NULL, " ");

                                // If NO WHERE
                                if (tokenMain == NULL)
                                {
                                    comArgs3[strlen(comArgs3) - 1] = '\0';
                                }
                                // If THERE's WHERE
                                else if (strcmp(tokenMain, "WHERE") == 0)
                                {
                                    // Column
                                    isWhere = 1;
                                    tokenMain = strtok(NULL, "=");
                                    if (tokenMain == NULL)
                                    {
                                        printf("ERROR : ARGS");
                                        send(new_socket, "ERROR : FORMAT\n", strlen("ERROR : FORMAT\n"), 0);
                                        continue;
                                    }
                                    // ERROR CHECKING
                                    sprintf(comArgs4, "%s", tokenMain);
                                    tokenMain = strtok(NULL, ";");
                                    if (tokenMain == NULL)
                                    {
                                        printf("ERROR : ARGS");
                                        send(new_socket, "ERROR : FORMAT\n", strlen("ERROR : FORMAT\n"), 0);
                                        continue;
                                    }
                                    // ERROR CHECKING
                                    sprintf(comArgs5, "%s", tokenMain);
                                }
                                // WHERE DETECT END

                                if (dml_update(curDB, comArgs1, comArgs2, comArgs3, isWhere, comArgs4, comArgs5) == 0)
                                {
                                    // SUCCESS
                                    send(new_socket, "SUCCESS\n", strlen("SUCCESS\n"), 0);
                                }
                                // ERROR
                                else
                                {
                                    send(new_socket, "ERROR EXEC\n", strlen("ERROR EXEC\n"), 0);
                                }
                            }
                        }
                        else
                        {
                            tokenMain = strtok(NULL, " ");
                            tokenMain = strtok(NULL, " ");
                            if (tokenMain == NULL)
                            {
                                printf("ERROR : ARGS");
                                send(new_socket, "ERROR : FORMAT\n", strlen("ERROR : FORMAT\n"), 0);
                                continue;
                            }
                            // ERROR CHECKING
                            sprintf(comArgs1, "%s", tokenMain);
                            tokenMain = strtok(NULL, ";");
                            if (tokenMain == NULL)
                            {
                                printf("ERROR : ARGS");
                                send(new_socket, "ERROR : FORMAT\n", strlen("ERROR : FORMAT\n"), 0);
                                continue;
                            }
                            // ERROR CHECKING
                            sprintf(comArgs2, "%s", tokenMain);
                            if (dml_insert(curDB, comArgs1, comArgs2) == 0)
                            {
                                // SUCCESS
                                send(new_socket, "SUCCESS\n", strlen("SUCCESS\n"), 0);
                            }
                            // ERROR
                            else
                            {
                                send(new_socket, "ERROR EXEC\n", strlen("ERROR EXEC\n"), 0);
                            }
                        }
                    }
                    else
                    {
                        char args[100][200];
                        int argsAm = 0;
                        int stillArg = 1;
                        int isAll = 0;
                        int isWhere = 0;
                        tokenMain = strtok(NULL, " ");
                        while (tokenMain != NULL)
                        {

                            if (strcmp(tokenMain, "FROM") == 0)
                            {
                                stillArg = 0;
                                tokenMain = strtok(NULL, " ");
                                // SENDS ERROR using stillArg
                                if (tokenMain == NULL)
                                {
                                    stillArg = 0;
                                    break;
                                }
                                // Gets DB name
                                sprintf(comArgs1, "%s", tokenMain);
                                tokenMain = strtok(NULL, " ");
                                // If NO WHERE
                                if (tokenMain == NULL)
                                {
                                    comArgs1[strlen(comArgs1) - 1] = '\0';
                                }
                                // If THERE's WHERE
                                else if (strcmp(tokenMain, "WHERE") == 0)
                                {
                                    // Column
                                    isWhere = 1;
                                    tokenMain = strtok(NULL, "=");
                                    if (tokenMain == NULL)
                                    {
                                        printf("ERROR : ARGS");
                                        send(new_socket, "ERROR : FORMAT\n", strlen("ERROR : FORMAT\n"), 0);
                                        continue;
                                    }
                                    // ERROR CHECKING
                                    sprintf(comArgs2, "%s", tokenMain);
                                    tokenMain = strtok(NULL, ";");
                                    if (tokenMain == NULL)
                                    {
                                        printf("ERROR : ARGS");
                                        send(new_socket, "ERROR : FORMAT\n", strlen("ERROR : FORMAT\n"), 0);
                                        continue;
                                    }
                                    // ERROR CHECKING
                                    sprintf(comArgs3, "%s", tokenMain);
                                }
                                // WRONG
                                else
                                {
                                    stillArg = 0;
                                }
                                break;
                            }
                            else if (strcmp(tokenMain, "*") == 0)
                            {
                                stillArg = 0;
                                isAll = 1;
                            }
                            if (stillArg)
                            {
                                strcpy(args[argsAm], tokenMain);

                                if (args[argsAm][strlen(args[argsAm]) - 1] == ',')
                                    args[argsAm][strlen(args[argsAm]) - 1] = '\0';
                                argsAm++;
                                tokenMain = strtok(NULL, " ");
                            }
                            else
                            {
                                tokenMain = strtok(NULL, " ");
                            }
                        }
                        if (stillArg)
                        {
                            send(new_socket, "ERROR : FORMAT\n", strlen("ERROR : FORMAT\n"), 0);
                            continue;
                        }

                        dml_select(curDB, comArgs1, isAll, args, isWhere, comArgs2, comArgs3, sendS);

                        send(new_socket, sendS, strlen(sendS), 0);
                    }
                }
                else
                {
                    if (sudof == 1)
                    {
                        tokenMain = strtok(NULL, " ");
                        if (tokenMain == NULL || strcmp(tokenMain, "PERMISSION") != 0)
                        {
                            printf("ERROR : ARGS");
                            send(new_socket, "ERROR : FORMAT\n", strlen("ERROR : FORMAT\n"), 0);
                            continue;
                        }
                        tokenMain = strtok(NULL, " ");
                        if (tokenMain == NULL)
                        {
                            printf("ERROR : ARGS");
                            send(new_socket, "ERROR : FORMAT\n", strlen("ERROR : FORMAT\n"), 0);
                            continue;
                        }
                        sprintf(comArgs1, "%s", tokenMain);
                        tokenMain = strtok(NULL, " ");
                        if (tokenMain == NULL || strcmp(tokenMain, "INTO") != 0)
                        {
                            printf("ERROR : ARGS");
                            send(new_socket, "ERROR : FORMAT\n", strlen("ERROR : FORMAT\n"), 0);
                            continue;
                        }
                        tokenMain = strtok(NULL, " ");
                        if (tokenMain == NULL)
                        {
                            printf("ERROR : ARGS");
                            send(new_socket, "ERROR : FORMAT\n", strlen("ERROR : FORMAT\n"), 0);
                            continue;
                        }
                        sprintf(comArgs2, "%s", tokenMain);
                        comArgs2[strlen(comArgs2) - 1] = '\0';

                        int check = checkUsernameExists(comArgs2);
                        if (check == 1)
                        {
                            addPermission(comArgs2, comArgs1);
                            strcpy(sendS, "Server : Permission Granted");
                            send(new_socket, sendS, strlen(sendS), 0);
                        }
                        else
                        {
                            strcpy(sendS, "Server : User doesn't exist");
                            send(new_socket, sendS, strlen(sendS), 0);
                        }
                    }
                    else if (sudof == 0)
                    {
                        strcpy(sendS, "Server : Access Denied");
                        send(new_socket, sendS, strlen(sendS), 0);
                    }
                }
            }
            else
            {
                tokenMain = strtok(NULL, ";");
                if (tokenMain == NULL)
                {
                    printf("ERROR : ARGS");
                    send(new_socket, "ERROR : FORMAT\n", strlen("ERROR : FORMAT\n"), 0);
                    continue;
                }
                // ERROR CHECKING
                sprintf(comArgs1, "%s", tokenMain);
                // Permission Checking
                if (sudof)
                    ;
                else if (!checkDbPermission(username, comArgs1))
                {
                    printf("ERROR : DB ACCESS DENIED");
                    send(new_socket, "ERROR : DB ACCESS DENIED\n", strlen("ERROR : DB ACCESS DENIED\n"), 0);
                    continue;
                }
                if (db_use(curDB, comArgs1) == 0)
                {
                    // SUCCESS
                    send(new_socket, "DB CHANGED\n", strlen("DB CHANGED\n"), 0);
                }
                else
                {
                    // WARNING : DIR DOESN'T EXIST
                    send(new_socket, "WARNING : DIR DOESN'T EXIST\n", strlen("WARNING : DIR DOESN'T EXIST\n"), 0);
                }
            }
        }
        else
        {
            tokenMain = strtok(NULL, " ");
            if (tokenMain == NULL)
            {
                printf("ERROR : ARGS");
                send(new_socket, "ERROR : FORMAT\n", strlen("ERROR : FORMAT\n"), 0);
                continue;
            }
            sprintf(command2, "%s", tokenMain);
            if (strcmp(command2, "TABLE") != 0)
            {
                if (strcmp(command2, "DATABASE") != 0)
                {
                    if (strcmp(command2, "USER") != 0)
                    {
                        printf("ERROR : UNKNOWN CREATE COMMAND\n");
                        send(new_socket, "ERROR : FORMAT\n", strlen("ERROR : FORMAT\n"), 0);
                        continue;
                    }
                    else
                    {

                        tokenMain = strtok(NULL, " ");
                        if (tokenMain == NULL)
                        {
                            printf("ERROR : ARGS");
                            send(new_socket, "ERROR : FORMAT\n", strlen("ERROR : FORMAT\n"), 0);
                            continue;
                        }
                        sprintf(comArgs1, "%s", tokenMain);
                        tokenMain = strtok(NULL, " ");
                        if (tokenMain == NULL || strcmp(tokenMain, "IDENTIFIED") != 0)
                        {
                            printf("ERROR : ARGS");
                            send(new_socket, "ERROR : FORMAT\n", strlen("ERROR : FORMAT\n"), 0);
                            continue;
                        }
                        tokenMain = strtok(NULL, " ");
                        if (tokenMain == NULL || strcmp(tokenMain, "BY") != 0)
                        {
                            printf("ERROR : ARGS");
                            send(new_socket, "ERROR : FORMAT\n", strlen("ERROR : FORMAT\n"), 0);
                            continue;
                        }
                        tokenMain = strtok(NULL, " ");
                        if (tokenMain == NULL)
                        {
                            printf("ERROR : ARGS");
                            send(new_socket, "ERROR : FORMAT\n", strlen("ERROR : FORMAT\n"), 0);
                            continue;
                        }
                        sprintf(comArgs2, "%s", tokenMain);
                        if (sudof == 0)
                        {
                            strcpy(sendS, "Server : Access Denied");
                            send(new_socket, sendS, strlen(sendS), 0);
                        }
                        else if (sudof == 1)
                        {
                            comArgs2[strlen(comArgs2) - 1] = '\0'; // Erase semicolon
                            createUser(comArgs1, comArgs2);
                            strcpy(sendS, "Server : USER CREATED");
                            send(new_socket, sendS, strlen(sendS), 0);
                        }
                    }
                }
                else
                {
                    tokenMain = strtok(NULL, ";");
                    if (tokenMain == NULL)
                    {
                        printf("ERROR : ARGS");
                        send(new_socket, "ERROR : FORMAT\n", strlen("ERROR : FORMAT\n"), 0);
                        continue;
                    }
                    // ERROR CHECKING
                    sprintf(comArgs1, "%s", tokenMain);
                    if (!db_create(comArgs1))
                    {
                        strcpy(sendS, "Server : Database Created");
                        send(new_socket, sendS, strlen(sendS), 0);
                        addPermission(username, comArgs1);
                    }
                    else
                    {
                        strcpy(sendS, "Server : ERROR EXEC");
                        send(new_socket, sendS, strlen(sendS), 0);
                    };
                }
            }
            else
            {
                tokenMain = strtok(NULL, " ");
                if (tokenMain == NULL)
                {
                    printf("ERROR : ARGS");
                    send(new_socket, "ERROR : FORMAT\n", strlen("ERROR : FORMAT\n"), 0);
                    continue;
                }
                // ERROR CHECKING
                sprintf(comArgs1, "%s", tokenMain);
                tokenMain = strtok(NULL, ";");
                if (tokenMain == NULL)
                {
                    printf("ERROR : ARGS");
                    send(new_socket, "ERROR : FORMAT\n", strlen("ERROR : FORMAT\n"), 0);
                    continue;
                }
                // ERROR CHECKING
                sprintf(comArgs2, "%s", tokenMain);
                if (!table_create(curDB, comArgs1, comArgs2))
                {
                    strcpy(sendS, "Server : Table Created");
                    send(new_socket, sendS, strlen(sendS), 0);
                }
                else
                {
                    strcpy(sendS, "Server : ERROR EXEC");
                    send(new_socket, sendS, strlen(sendS), 0);
                };
            }
        }
    } while (isExit);
    return 0;
}