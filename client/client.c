#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#define PORT 8080

int main(int argc, char const *argv[])
{
    int sudo = 0;
    int isSetDB = 0;
    if (getuid() == 0)
    {
        printf("sudo client\n");
        sudo = 1;
        if(argc == 2){
            printf("Format: %s -d [database]\n", argv[0]);
            return 1;
        }
        else if(argc == 3){
            isSetDB = 1;
        }

    }
    else if (argc < 5)
    {
        printf("Format: %s -u <username> -p <password>\n", argv[0]);
        return 1;
    }
    else if (argc == 6){
        printf("Format: %s -u [username] -p [password] -d [database]\n", argv[0]);
        return 1;
    }
    else if (argc == 7)
    {
        isSetDB = 1;
    }

    struct sockaddr_in address;
    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    char user[1024];
    char pass[1024];
    strcpy(user, argv[2]);
    strcpy(pass, argv[4]);
    char buffer[1024] = {0};
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("\n Socket creation error \n");
        return -1;
    }

    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0)
    {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        printf("\nConnection Failed \n");
        return -1;
    }

    int sudoVal = htonl(sudo);
    send(sock, &sudoVal, sizeof(sudoVal), 0);
    valread = read(sock, buffer, 1024);
    printf("%s\n", buffer);
    memset(buffer, 0, sizeof(buffer));

    if (sudo == 1)
    {

        while (1)
        {
            char input[1024];
            printf("Client: ");
            scanf(" %[^\n]s", input);
            send(sock, input, strlen(input), 0);
            // terima
            valread = read(sock, buffer, 1024);
            printf("%s\n", buffer);
            if (strcmp(buffer, "EXIT") == 0)
            {
                printf("Sudo Exit\n");
                return 0;
            }
            memset(buffer, 0, sizeof(buffer));
        }
    }
    else if (sudo == 0)
    {
        // send username
        send(sock, user, strlen(user), 0);
        valread = read(sock, buffer, 1024);
        printf("%s\n", buffer);
        memset(buffer, 0, sizeof(buffer));

        // send password
        send(sock, pass, strlen(pass), 0);
        valread = read(sock, buffer, 1024);
        if (strcmp(buffer, "FAILED") == 0)
        {
            printf("Failed to Authenticate\n");
            return 0;
        }
        printf("%s\n", buffer);
        memset(buffer, 0, sizeof(buffer));
        if (isSetDB)
        {
            char useDB[1024];
            if(sudo){
                sprintf(useDB, "USE %s;", argv[2]);
            }
            else{
                sprintf(useDB, "USE %s;", argv[6]);
            }
            
            
            send(sock, useDB, strlen(useDB), 0);
        }
        else
            printf("IMPORTANT : Please use the \"USE\" command to pick a database first or create a new one\nALSO : use \"EXIT;\" to exit client and database\n");
        while (1)
        {
            char input[1024];
            printf("Client: ");
            scanf(" %[^\n]s", input);
            send(sock, input, strlen(input), 0);
            // terima
            valread = read(sock, buffer, 1024);
            printf("%s\n", buffer);
            if (strcmp(buffer, "EXIT") == 0)
            {
                printf("%s Exit\n", argv[2]);
                return 0;
            }
            memset(buffer, 0, sizeof(buffer));
        }
    }

    return 0;
}
